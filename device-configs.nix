{
  cameron-laptop = {
    window-manager = "xmonad";
  };
  cameron-laptop-2 = {
    window-manager = "xmonad";
  };
  cameron-phone = {
    window-manager = "phosh";
  };
  cameron-pine = {
    window-manager = "phosh";
  };
}
